//
//  RecorridoModel.swift
//  test-app-iOS
//
//  Created by ServitrackGPS on 12/1/17.
//  Copyright © 2017 ServitrackGPS. All rights reserved.
//

import Foundation

class Recorrido {
    var latitud: Double?;
    var longitud: Double?;
    var tiempo: Date?;
}
