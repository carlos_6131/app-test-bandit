//
//  HistoricoController.swift
//  test-app-iOS
//
//  Created by ServitrackGPS on 12/1/17.
//  Copyright © 2017 ServitrackGPS. All rights reserved.
//

import UIKit
import CoreData

class HistoricoController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var historicoRecorrido: [Locations] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.historicoRecorrido = queryLocations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = String(historicoRecorrido[indexPath.row].latitud)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historicoRecorrido.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func queryLocations() -> [Locations] {
       
        let comandoFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Locations")
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do{
            let comandoFetched = try context.fetch(comandoFetch) as! [Locations]
            self.tableView.reloadData()
            return comandoFetched
        }
        catch{
            fatalError("Error al consultar")
        }
        
    }
    
    

}
