//  ViewController.swift
//  test-app-iOS
//
//  Created by ServitrackGPS on 12/1/17.
//  Copyright © 2017 ServitrackGPS. All rights reserved.

import UIKit
import GoogleMaps
import CoreData

class RecorridoController: UIViewController, CLLocationManagerDelegate {

    var locationManager = CLLocationManager()
    @IBOutlet weak var speed: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: 6.3427239,longitude:-66.0501576, zoom:5)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            mapView.isMyLocationEnabled = true
        self.view = mapView
        verifyAuthorizationLocation()
        
    }
    
    func verifyAuthorizationLocation(){
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // cuando el permiso de localizacion cambia
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status:CLAuthorizationStatus) {
        
        var hasAuthorised = false
        var locationStatus:NSString = ""
        
        switch status {
            case CLAuthorizationStatus.restricted: locationStatus = "Restricted Access"
            case CLAuthorizationStatus.denied: locationStatus = "Denied access"
            case CLAuthorizationStatus.notDetermined: locationStatus = "Not determined"
            default: locationStatus = "Allowed access"
                     hasAuthorised = true
        }
        
        if(!hasAuthorised) {
            print("locationStatus \(locationStatus)")
        }
    }
    
    private func saveNewCoordinate2D(location:CLLocationCoordinate2D, time: Date){
        
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.insertNewObject(forEntityName: "Locations", into: context)
        
        entity.setValue(location.latitude, forKey: "latitud")
        entity.setValue(location.longitude, forKey: "longitud")
        entity.setValue(time, forKey: "tiempo")

        do{
            try context.save()
        }
        catch{
            fatalError("Error al insertar")
        }
    }
    
    // Metodo Delegado que se ejecuta cuando la ubicacion del disposivo cambia
    private func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        
        let time = Date()
        saveNewCoordinate2D(location: newLocation.coordinate, time: time)
        
    }
    
    private func calculateDistance(origin: CLLocation, to: CLLocation) -> Decimal {
        let distanceMeters = origin.distance(from: to)
        let distanceKM = distanceMeters / 1000
        return Decimal(distanceKM)
    }
    
    private func calculateSpeed(distance: Decimal, time:Decimal) -> Decimal{
        let speed = (distance/time)
        return speed
    }
    

}

