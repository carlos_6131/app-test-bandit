//
//  Locations+CoreDataProperties.swift
//  
//
//  Created by ServitrackGPS on 12/1/17.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension Locations {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Locations> {
        return NSFetchRequest<Locations>(entityName: "Locations");
    }

    @NSManaged public var latitud: Double
    @NSManaged public var longitud: Double
    @NSManaged public var tiempo: NSDate?

}
